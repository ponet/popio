def showMenu():
    print("1) Eat")
    print("2) Study")
    print("0) Exit")
    return None

def askChoice() -> int:
    Choice = -1
    Feed = input("Insert choice: ")
    try:
        Choice = int(Feed)
    except Exception:
        pass
    return Choice

def menu():
    Choice: int = int(-1)
    while Choice != 0:
        showMenu()
        Choice = askChoice()
        if Choice == 1:
            print("Eating.")
        elif Choice == 2:
            print("Studying.")
        elif Choice == 0:
            print("Exiting.")
        else:
            print("Unknown option.")
        print("")
    return None

menu()
