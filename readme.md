# POPIO

Run python3.11 program and store its stdin & stdout in sequential order.

## Setup

Prerequisites to follow the setup steps:

- Git
- Bash
- Python >=3.11

Here are steps to setup the popio tool:

1. Clone this repository with Git and navigate to it
2. Setup and activate `venv`
3. Install dependencies from `requirements.txt`
4. Bundle the `src/popio.py` into a single package with PyInstaller.
5. Test the executable in `dist` with the provided files `example.py` and `input.txt`. See usage below
6. Move the executable into your system and set the path.

Bash script for the setup process (see OS differences):

```bash
git clone https://gitlab.com/ponet/popio # 1. Clone repository
cd popio # Navigate to the project

python -m venv .venv # 2. Setup venv
# Note! use only the activation command which represents your OS
source .venv/bin/activate # Linux / MacOS
source .venv/Scripts/activate # Windows

pip install -r requirements.txt # 3. Install dependencies
pyinstaller --onefile src/popio.py # 4. Bundle

./dist/popio.exe --help # 4. Test
./dist/popio.exe example.py input.txt > output.txt # See output.txt
```

To use the `popio` from anywhere in your system, prefer to follow OS specific guides. Some tips below that may work for you:

1. Linux/MacOS
    1. Move the executable to `/usr/local/bin`
    2. Change the permission to `755`
2. Windows
    1. Create directory under home e.g. `~/.popio`
    2. Copy `popio.exe` to `~/.popio`
    3. Add `C:\Users\{username}\.popio` to the `Path` variable
        1. Search for "Environment variables for your account"

## Usage

Usage: `popio python_file.py input.txt > output.txt`

See also `popio --help` for more details.

